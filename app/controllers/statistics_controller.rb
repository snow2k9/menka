# frozen_string_literal: true

class StatisticsController < ApplicationController
  before_action :authenticate_user!

  def index
    events = current_user.events.where.not(end_time: nil, analyse: false).order(end_time: :asc)
    if events.present?
      @statistics = OpenStruct.new(
        chart_options: { scales: { yAxes: [{ ticks: { beginAtZero: true } }] } },
        duration: calc_duration(events),
        intensity: calc_intensity(events),
        interval: calc_interval(events)
      )
      @statistics
    else
      redirect_to events_path, notice: 'Keine Events zum Auswerten.'
    end
  end

  private

  def calc_duration(events)
    durations = events.map { |e| (e.end_time.to_i + 1 - e.start_time.to_i) / 60 / 60 / 24 }
    duration = durations.inject(:+).to_f / durations.size
    duration_labels = events.map { |e| t('date.month_names')[e.start_time.month] }

    {
      avg: duration.to_i,
      labels: duration_labels,
      datasets: [
        {
          label: t('menka.statistics.duration').to_s,
          backgroundColor: 'green',
          borderColor: 'rgba(220,220,220,1)',
          data: durations
        }
      ]
    }
  end

  def calc_intensity(events)
    intensities = events.map(&:intensity)
    intensity = intensities.inject(:+).to_f / intensities.size
    intensity_labels = events.map { |e| "#{e.start_time.strftime(t('time.formats.default'))} - #{e.end_time.strftime(t('time.formats.default'))}" }

    {
      avg: intensity.to_i,
      labels: intensity_labels,
      datasets: [
        {
          label: t('menka.statistics.intensity').to_s,
          backgroundColor: 'red',
          borderColor: 'rgba(220,220,220,1)',
          data: intensities
        }
      ]
    }
  end

  def calc_interval(events)
    intervals = []
    i = 0
    while i < events.size - 1
      intervals << ((events[i + 1].start_time - 1.day).to_i - events[i].end_time.to_i) / 60 / 60 / 24
      i += 1
    end
    # interval_labels = events.map { |e| t('date.month_names')[e.end_time.month] }
    {
      avg: (intervals.inject(:+).to_f / intervals.size).to_i,
      # labels: interval_labels,
      datasets: [
        {
          label: t('menka.statistics.interval').to_s,
          backgroundColor: 'blue',
          borderColor: 'rgba(220,220,220,1)',
          data: intervals
        }
      ]
    }
  end
end
