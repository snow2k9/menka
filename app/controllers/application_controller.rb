# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :set_locale
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[email password password_confirmation nickname locale])
    devise_parameter_sanitizer.permit(:account_update, keys: %i[email password password_confirmation nickname locale])
  end

  private

  def set_locale
    I18n.locale = current_user.try(:locale) || I18n.default_locale
  end
end
