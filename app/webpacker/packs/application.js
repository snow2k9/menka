require.context('../images/', true, /\.(gif|jpg|png|svg)$/i)
import Rails from 'rails-ujs';
import Turbolinks from 'turbolinks';
import Chart from 'chart.js';
import 'bulma-slider/dist/js/bulma-slider';

Rails.start();
Turbolinks.start();

import '../src/js/menka.js';


