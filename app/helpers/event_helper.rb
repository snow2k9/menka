# frozen_string_literal: true

module EventHelper
  def days_diff(from, to)
    ((from - to) / 60 / 60 / 24).round.abs
  end
end

module SimpleCalendar
  class Calendar
    def bulma_classes_for(day)
      # today = Date.current

      bulma_class = ['datepicker-date']
      # bulma_class << 'is-today'                if today == day
      bulma_class << 'is-current-month'        if start_date.month == day.month
      bulma_class << 'datepicker-range'        if sorted_events.fetch(day, []).any?
      bulma_class << 'datepicker-range-start'  if day.to_date == sorted_events.fetch(day, []).pluck(:start_time).map(&:to_date).uniq[0]
      bulma_class << 'datepicker-range-end' if sorted_events.fetch(day, []).pluck(:end_time).uniq.any? && day.to_date == sorted_events.fetch(day, []).pluck(:end_time).map(&:to_date).uniq[0]
      # bulma_class << 'prev-month'    if start_date.month != day.month && day < start_date
      # bulma_class << 'next-month'    if start_date.month != day.month && day > start_date
      # bulma_class << "wday-#{day.wday.to_s}"
      # bulma_class << 'past'          if today > day
      # bulma_class << 'future'        if today < day
      bulma_class
    end
  end
end
