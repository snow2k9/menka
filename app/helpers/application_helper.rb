# frozen_string_literal: true

module ApplicationHelper
  def locale_languages
    [
      [t('locales.en'), 'en'],
      [t('locales.de'), 'de']
    ]
  end
end
