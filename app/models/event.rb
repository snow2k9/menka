# frozen_string_literal: true

# == Schema Information
#
# Table name: events
#
#  id          :bigint(8)        not null, primary key
#  user_id     :bigint(8)
#  title       :string           not null
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  intensity   :integer          not null
#  start       :date
#  end         :date
#
# Indexes
#
#  index_events_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class Event < ApplicationRecord
  # enum intensity: %i[very_weak weak normal strong very_strong]
  belongs_to :user, inverse_of: :events

  after_initialize :set_default_values, only: :new_record?

  validates :title, presence: true
  validates :start_time, presence: true
  validates :intensity, presence: true

  private

  def set_default_values
    self.title = I18n.t 'menka.event.default_title'
  end
end
