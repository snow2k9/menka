# frozen_string_literal: true

class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.belongs_to :user, foreign_key: true
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
