# frozen_string_literal: true

class AddAnalyseToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :analyse, :boolean, default: true
  end
end
