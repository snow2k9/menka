# frozen_string_literal: true

class ChangeColumnNotNull < ActiveRecord::Migration[5.1]
  def change
    change_column_null :events, :start_time, false, Time.zone.now
    change_column_null :events, :title, false, 'Period'
    change_column_null :events, :intensity, false, 2

    change_column_null :users, :nickname, false, 'Nobody'
  end
end
