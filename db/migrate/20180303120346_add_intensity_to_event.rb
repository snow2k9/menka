# frozen_string_literal: true

class AddIntensityToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :intensity, :integer
  end
end
